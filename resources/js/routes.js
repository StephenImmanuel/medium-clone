import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import Home from './pages/post/All'
import ShowPost from './pages/post/Show'
import EditPost from './pages/post/Edit'
import CreatePost from './pages/post/Create'
import UserPost from './pages/post/UserPost'
import TaggedPost from './pages/post/TaggedPost'
import ApiException from './pages/ApiException'
import SignIn from './pages/user/SignIn'
import Register from './pages/user/Register'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/signin',
            name: 'signin',
            component: SignIn,
        },
        {
            path: '/register',
            name: 'register',
            component: Register,
        },
        {
            path: '/showPost/:id',
            name: 'showPost',
            component: ShowPost,
            props: true
        },
        {
            path: '/editPost',
            name: 'editPost',
            component: EditPost,
            props: true
        },
        {
            path: '/createPost',
            name: 'createPost',
            component: CreatePost,
            props: true
        },
        {
            path: '/myPosts',
            name: 'myPosts',
            component: UserPost,
            props: true
        },
        {
            path: '/taggedPosts',
            name: 'taggedPosts',
            component: TaggedPost,
            props: true
        },
        {
            path: '/apiException',
            name: 'apiException',
            component: ApiException,
            props: true
        }
    ],
});

export default router;
