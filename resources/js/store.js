import Vue from "vue";
import Vuex from "vuex";
import Axios from "axios";

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        authenticated: false,
        user: null,
        token: null
    },
    getters: {
        authenticated(state) {
            return state.authenticated == true;
        },
        user(state) {
            return state.user;
        },
        token(state) {
            return state.token;
        }
    },
    mutations: {
        setToken(state, token) {
            state.token = token;
        },
        login(state, user) {
            state.authenticated = true;
            state.user = user;
        },
        logout(state, user) {
            state.authenticated = false;
            state.user = "";
            state.token = "";
        }
    },
    actions: {
        login({ commit }, token) {
            commit("setToken", token);

            axios.get('/api/user').then(response => {
                commit('login', response.data)
                router.push('/myPosts')
            })
        },
        logout({ commit }) {
            axios.post("/api/logout").then(response => {
                commit("logout");
                router.push("/");
            });
        }
    }
});
