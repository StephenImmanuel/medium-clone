<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class TaggedPostController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke($name)
    {
        $posts = Post::with('user')->withAnyTags($name)->get();

        return $posts;
    }
}
