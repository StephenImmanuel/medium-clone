<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use \Spatie\Tags\HasTags;
    
    protected $fillable = ['title', 'sub_title', 'description', 'user_id'];
    protected $appends = ['tag_names'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getTagNamesAttribute()
    {
        return implode(',', $this->tags->pluck('name')->all());
    }
}
