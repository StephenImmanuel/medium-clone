# Installation & Starting Up

- [Laravel](https://laravel.com/docs/5.8) - Please follow link to install laravel dependencies
- Run `composer update` from root dir
- Run `npm install` from root dir
- copy `cp .env.sample .env` from root dir. Fill in the details of the database
- Run `php artisan storage:link` from the root dir. This is for uploading images.
- Run `php artisan migrate:refresh --seed` to create migrations and seed the database
- Run `php artisan serve` from root dir
- Run `php artisan passport:client --personal` from root dir. Type 'Y' upon conformation.

# Running Tests

- Set the `APP_URL=http://localhost:8000` in the .env file. Update the port if different
- Run `php artisan dusk` from root dir

# Users

- User `admin@test.com` and password is `password`
- User `admin2@test.com` and password is `password`

# Tasks

[X] Admin panel to post articles.
[X] Ability to add one or more photos to the article.
[X] Tag Articles
[X] Frontend to list and display article.
[X] Database migrations and seeds (Use Faker library to create dummy data)
[X] CRUD and Resource Controllers
[X] Form Validation and Requests
[X] Use of VueJS / Jquery on frontend
[X] Tailwindcss as a CSS framework.
[X] Testing