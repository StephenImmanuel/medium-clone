<?php

use App\Post;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::post('login', 'AuthController@login')->name('login');
Route::post('register', 'AuthController@register')->name('register');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('tags', function () {
    return Spatie\Tags\Tag::all();
});

Route::middleware('auth:api')->group(function () {
    Route::resource('posts', 'PostController')->except(['index', 'show']);
    Route::get('userPosts', 'UserPostController');
    Route::post('logout', 'AuthController@logout')->name('logout');
});

Route::get('/taggedPosts/{name}', 'TaggedPostController');
Route::resource('upload', 'ImageUploadController')->only(['store', 'destroy']);
Route::resource('posts', 'PostController')->only(['index', 'show']);
