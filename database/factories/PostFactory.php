<?php

use Faker\Generator as Faker;
use App\Post;

$factory->define(Post::class, function (Faker $faker) {

    $tags = ['technology', 'business', 'health', 'computer', 'AI', 'science'];

    return [
        'title' => $faker->sentence(3),
        'sub_title' => $faker->sentence(6),
        'description' => $faker->paragraph(6),
        'tags' => $faker->randomElements($tags, 2, false)
    ];
});
