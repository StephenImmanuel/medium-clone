<?php

use App\Post;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create(['email' => 'admin@test.com']);
        factory(User::class)->create(['email' => 'admin2@test.com']);

        factory(User::class, 19)->create();

        User::all()->each(function ($user) {
            $user->posts()->save(factory(Post::class)->make());
        });
    }
}
