<?php

namespace Tests\Browser;

use App\Post;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use Illuminate\Foundation\Testing\WithFaker;

class MediumCloneTest extends DuskTestCase
{
    use withFaker;

    public function testHomepage()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                ->waitForText('Sign in')
                ->assertSee('Become a member')
                ->assertSee('Tags')
                ->assertSee('#computer')
                ->assertDontSee('New Post')
                ->assertDontSee('My Posts')
                ->assertDontSee('Logout');

            $this->assertEquals(21, count($browser->elements('.user-posts')));
        });
    }

    public function testReadPost()
    {
        $this->browse(function (Browser $browser) {
            $browser->press('.user-posts')
                ->waitFor('.show-post')
                ->assertDontSee('Delete')
                ->assertDontSee('Edit');
        });
    }

    public function testEditOrDeleteNotAvailableWhenNotLogged()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/showPost/1')
                ->waitForLocation('/showPost/1')
                ->assertDontSee('Delete')
                ->assertDontSee('Edit');
        });
    }

    public function testLogin()
    {
        $this->browse(function (Browser $browser) {
            $browser->clickLink('Sign in')
                ->assertPathIs('/signin')
                ->assertSee('Login')
                ->type('email', 'admin@test.com')
                ->type('password', 'password')
                ->press('button#login')
                ->waitForLocation('/myPosts')
                ->waitForText('Logout')
                ->assertSee('New Post')
                ->assertSee('My Posts')
                ->assertSee('Logout')
                ->assertDontSee('Sign in')
                ->assertDontSee('Become a member');
        });
    }

    public function testUserPostsList()
    {
        $this->browse(function (Browser $browser) {
            $this->assertEquals(1, count($browser->elements('.user-posts')));
        });
    }

    public function testUserSeeOthersPostsList()
    {
        $this->browse(function (Browser $browser) {
            $browser->clickLink('Medium')
            ->waitForLocation('/')
            ->assertSee('Tags')
            ->waitFor('.user-posts');

            $this->assertEquals(21, count($browser->elements('.user-posts')));
        });
    }

    public function testReadUserPost()
    {
        $this->browse(function (Browser $browser) {
            $browser
                ->clickLink('My Posts')
                ->waitForLocation('/myPosts')
                ->waitFor('.user-posts')
                ->press('.user-posts')
                ->waitForLocation('/showPost/1')
                ->assertSee('Edit')
                ->assertSee('Delete');
        });
    }

    public function testUpdateUserPost()
    {
        $this->browse(function (Browser $browser) {
            $browser->press('button#edit')
                ->waitForLocation('/editPost')
                ->assertSee('Save')
                ->type('sub_title', 'My new subtitle')
                ->press('button#save')
                ->waitForLocation('/myPosts')
                ->waitFor('.user-posts')
                ->assertSee('My new subtitle');
        });
    }

    public function testNewUserPost()
    {
        $this->browse(function (Browser $browser) {
            $browser->clickLink('New Post')
                ->waitForLocation('/createPost')
                ->press('button#save')
                ->waitForLocation('/createPost')
                ->waitForText('The title field is required')
                ->waitForText('The sub title field is required')
                ->waitForText('The tag names field is required')
                ->waitForText('The description field is required')
                ->type('title', 'Browser test post')
                ->type('sub_title', 'Creating post from browser')
                ->keys('div.ck-editor__editable', 'Description by test')
                ->type('tags', 'test')
                ->press('Save')
                ->waitForLocation('/myPosts')
                ->waitFor('.user-posts')
                ->assertSee('Creating post from browser')
                ->assertSee('Browser test post');

            $this->assertEquals(2, count($browser->elements('.user-posts')));
        });
    }

    public function testDeleteUserPost()
    {
        $this->browse(function (Browser $browser) {
            $post = Post::latest()->first();

            $browser
                ->assertPathIs('/myPosts')
                ->waitFor('.user-posts')
                ->press('.user-posts')
                ->waitForText('Delete')
                ->press('button#delete')
                ->waitForLocation('/')
                ->waitFor('.user-posts');

            $this->assertEquals(21, count($browser->elements('.user-posts')));
        });
    }


    public function testEditOrDeleteNotAvailableForOtherUserPostWhenLogged()
    {
        $this->browse(function (Browser $browser) {
            $browser->clickLink('Medium')
                ->waitFor('.user-posts')
                ->click('.post-5')
                ->waitFor('.show-post')
                ->assertDontSee('Edit')
                ->assertDontSee('Delete');
        });
    }

    public function testLogout()
    {
        $this->browse(function (Browser $browser) {
            $browser->clickLink('Logout')
                ->waitForText('Tags')
                ->assertPathIs('/');
        });
    }

    public function testRegistration()
    {
        $this->browse(function (Browser $browser) {
            $name = $this->faker->name;
            $email = $this->faker->unique()->safeEmail;

            $browser->clickLink('Become a member')
                ->waitForLocation('/register')
                ->waitFor('button#register')
                ->press('button#register')
                ->waitFor('button#register')
                ->assertSee('The name field is required')
                ->assertSee('The email field is required')
                ->assertSee('The password field is required')
                ->press('button#register')
                ->waitFor('button#register')
                ->type('name', $name)
                ->type('email', $email)
                ->type('password', 'password')
                ->type('password_confirmation', 'password')
                ->press('button#register')
                ->waitForLocation('/signin')
                ->waitFor('button#login')
                ->type('email', $email)
                ->type('password', 'password')
                ->click('button#login')
                ->waitForLocation('/myPosts')
                ->waitForText('You dont have any posts');
        });
    }
}
